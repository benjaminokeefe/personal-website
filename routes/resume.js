const
  express = require('express'),
  router = express.Router(),
  data = require('../data/resume');

router.get('/', function(req, res, next) {
  res.render('resume', data);
});

module.exports = router;
