const
  gulp = require('gulp'),
  less = require('gulp-less'),
  cssmin = require('gulp-cssmin'),
  rename = require('gulp-rename'),
  path = require('path');

gulp.task('watch', function () {
  gulp.watch('./styles/*.less', ['less']);
});

gulp.task('less', function () {

  return gulp.src('./styles/style.less')
  .pipe(less().on('error', function (err) {
    console.log(err);
  }))
  .pipe(cssmin().on('error', function(err) {
    console.log(err);
  }))
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('./public/stylesheets'))
});

gulp.task('default', ['less', 'watch']);
