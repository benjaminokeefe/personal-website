module.exports = {
  name: 'Benjamin O\'Keefe',
  phone: '3016399497',
  email: 'benjaminokeefe@gmail.com',
  website: 'http://www.benokeefe.com',
  summary: 'Driven and dependable software engineer with an enthusiastic interest in the design, development, and synthesis of client and server web technologies. Values include software that is simple, elegant, maintainable, and is designed and written under the assumption that if one does not fight for simplicity, one will get complexity.',
  technicalSkills: [
    'JavaScript',
    'React',
    'React Router',
    'Ember.js',
    'Ember Data',
    'Node.js',
    'Express.js',
    'REST',
    'jQuery',
    'HTML',
    'LESS',
    'CSS',
    'C#',
    'ASP.NET Web Forms/MVC',
    'MongoDB',
    'RDBMS',
    'Git',
    'Agile/Scrum',
    'Cloud Services'
  ],
  personalProjects: [
    {
      projectName: 'Kanji Flash',
      experiences: [
        'Designed, implemented, deployed, and currently maintain and improve <a href="http://www.kanjiflash.com" target="_blank">Kanji Flash</a>, a modularized web application that assists Japanese language students in learning kanji characters',
        'Created an Ember CLI client application (<a href="https://bitbucket.org/benjaminokeefe/kanji-flash-client/src" target="_blank">view code</a>)',
        'Created an Express.js server application (<a href="https://bitbucket.org/benjaminokeefe/kanji-flash-server/src" target="_blank">view code</a>)',
        'Created a REST API, which communicates with a MongoDB document database for data serialization (<a href="https://bitbucket.org/benjaminokeefe/kanji-flash-api/src" target="_blank">view code</a>)',
      ]
    }
  ],
  professionalExperience: [
    {
      companyName: 'INRIX - OpenCar',
      title: 'Platform Engineer',
      startDate: 'May 2016',
      endDate: 'October 2017',
      experiences: [
        'Designing, implementing, and improving core components of the OpenCar framework, an in-car infotainment system',
        'Researching and implementing a Scrum process and acting as Scrum Master for the OpenCar Development Team',
        'Leading Scrum-related activities, including daily standup, sprint planning, sprint review, and sprint retrospective meetings',
        'Writing and reviewing RFC documents to propose new APIs for the OpenCar framework',
        'Writing unit tests to bring code coverage of core OpenCar framework modules to 80+% coverage'
      ]
    },
    {
      companyName: 'Staples',
      title: 'Software Developer III',
      startDate: 'Feb 2016',
      endDate: 'May 2016, 3-month contract',
      experiences: [
        'Working on an Agile team to improve SEO for <a href="http://www.staples.com" target="_blank">www.staples.com</a>, the third largest online retailer in the United States',
        'Creating a new Node.js API for interacting with a Couchbase database'
      ]
    },
    {
      companyName: 'Sublime Media',
      title: 'Software Engineer',
      startDate: 'Jul 2014',
      endDate: 'Jan 2016',
      experiences: [
        'Architecting and implementing a re-design of the front-end application for a major pharmaceutical client’s online learning portal using Ember.js and Ember Data',
        'Working directly with clients to gather and clarify requirements, and to address feedback and concerns about projects'
      ]
    },
    {
      companyName: 'Triad Interactive',
      title: 'Web Applications Engineer',
      startDate: 'Apr 2009',
      endDate: 'Jul 2014',
      experiences: [
        'Debugging, improving, and maintaining SimNet - the McGraw Hill company’s premier online secondary education learning portal for Microsoft Office products',
        'Implementing and maintaining SimGrader - an online document processing grading engine using C#, LINQ, and XML',
      ]
    },
    {
      companyName: 'Westat',
      title: 'Programmer Analyst',
      startDate: 'Jul 2007',
      endDate: 'Apr 2009',
      experiences: [
        'Developing and maintaining legacy ASP 3.0 online data management systems',
        'Investigating requirements to convert legacy systems to ASP.NET',
      ]
    }
  ],
  education: [
    {
      schoolName: 'Shepherd University',
      experiences: [
        'Graduated Cum Laude (3.6 GPA) with a B.S. in Computer Sciences'
      ]
    }
  ],
  certifications: [
    {
      certificationName: 'Professional Scrum Developer (PSD-I)'
    }
  ],
  languages: [
    'English (native)',
    'Japanese (approximately JLPT N2 level)'
  ]
};
